
# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
from buildbot.process.factory import BuildFactory
from buildbot.steps.shell import ShellCommand
from buildbot.plugins import util
from buildbot.plugins import steps
Property = util.Property
Git = steps.Git

class GitSource:
    def __init__(self,url,ref,sha=""):
        ''' Specify a source by a url and a ref. Optionally a SHA '''
        self.url = url
        self.ref = ref
        self.sha = sha

class Step:
    def __init__(self,name,trigger,properties,timeout=1200,get_definitions=False):
        self.name = name
        self.trigger = trigger
        self.properties = properties
        self.timeout = timeout
        self.get_definitions = get_definitions

Build = Step(
        name = 'Build',
        trigger = 'builder_trigger.sh',
        properties = [
                ("ref","cu010-trove/br6/firehose-test-1"),
                ("sha","HEAD"),
                ("system",'genivi-demo-platform-x86_64-generic.morph'),
                ("pipeline","no pipeline given")],
        timeout = 7200,
        get_definitions = True)
Images = Step(
        name = 'Image(s)',
        trigger = 'deploy_trigger.sh',
        properties = [
                ('system','no system give'),
                ('buildnumber',0),
                ('buildslave_scripts_sha','no buildslave-scripts SHA given'),
                ('definitions_sha','no definitions SHA given'),
                ('testing_sha','no testing SHA given'),
                ("pipeline","no pipeline given")],
        timeout = 1800,
        get_definitions = True)
Test = Step(
        name = 'Test',
        trigger = 'testing_trigger.sh',
        properties = [
                ('artefact','no artefact given'),
                ('testing_sha','no testing SHA given'),
                ('buildslave_scripts_sha','no buildslave-scripts SHA given'),
                ('definitions_sha','no definitions SHA given'),
                ("pipeline","no pipeline given")])
Publish = Step(
        name = 'Publish',
        trigger = 'publish_trigger.sh',
        properties = [
                ('artefact','no artefact given'),
                ('testing_sha','no testing SHA given'),
                ('buildslave_scripts_sha','no buildslave-scripts SHA given'),
                ('definitions_sha','no definitions SHA given'),
                ("pipeline","no pipeline given")])

Steps = {
        "Build": Build,
        "Image(s)": Images,
        "Test": Test,
        "Publish": Publish}

class Column:

    def add_get_definitions(self,ref):
        ''' add a step fetch definitions '''

        ref = Property("ref",default=ref)
        get_defns_cmd = ['sh','get_definitions.sh',ref]
        shell_cmd = ShellCommand(command=get_defns_cmd,
                                 timeout=self.timeout)
        self.factory.addStep(shell_cmd)

    def format_cmd(self):
        ''' a buildbot shell command to pass the properties to trigger '''

        util_properties = []
        for property in self.properties:
            name = property[0]
            default_str = property[1]
            util_property = Property(name,default=default_str)
            util_properties.append(util_property)
        cmd = ['sh',self.trigger]+util_properties
        return ShellCommand(command=cmd,
                            timeout=self.timeout)

    def __init__(self,
                 name,
                 source_repo,
                 category,
                 trigger,
                 slavenames,
                 properties,
                 timeout=1200,
                 get_definitions=False):
        ''' A worker in CIAT Orchestration which appears as a column in the
            buildbot waterfall. get_definitions is either False or a ref '''

        self.name = name
        assert isinstance(source_repo,GitSource)
        self.source_repo = source_repo
        self.category = category
        self.trigger = 'triggers/%s' % trigger
        self.slavenames = slavenames
        self.properties = properties
        self.timeout = timeout
        self.get_definitions = get_definitions
        self.factory = BuildFactory()
        self.factory.addStep(Git(
                repourl=self.source_repo.url,
                branch=self.source_repo.ref,
                mode='incremental'))
        if self.get_definitions:
            self.add_get_definitions(get_definitions)
        self.cmd = self.format_cmd()
        self.factory.addStep(self.cmd)

class Pipeline:

    def get_slaves(self,slave_type):
        ''' this returns a list of slaves given a slave-type '''
        #TODO this needs doing properly
        if slave_type == 'arm-slave':
            return 'arm-slave'
        return 'local-slave'

    def __init__(self,
            name,
            candidate_refs,
            slave_type,
            clusters,
            steps):
        self.name = name
        self.candidate_refs = candidate_refs
        self.slavenames = self.get_slaves(slave_type)
        self.clusters = clusters
        self.steps = steps
        self.categories = []
        self.columns = []
        
        BUILD_SLAVE_SCRIPTS = GitSource(
                'ssh://git@git.baserock.org/baserock/ciat/buildslave-scripts.git',
                'master')

        for step in steps:
            column_name = "%s %s" % (self.name, step.name)
            category = column_name
            self.categories.append(category)
            if step.get_definitions:
                get_definitions = self.candidate_refs[0]
            self.columns.append(Column(
                    name = column_name,
                    source_repo = BUILD_SLAVE_SCRIPTS,
                    category = category,
                    trigger = step.trigger,
                    slavenames = [self.slavenames],
                    properties = step.properties,
                    timeout = step.timeout,
                    get_definitions = get_definitions))

def pipeline_from_dict(_dict):
    ''' given a dict of a pipeline return an object '''
    name = _dict['name']
    candidate_refs = _dict['candidate-refs']
    slave_type = _dict['slave-type']
    clusters = _dict['clusters']
    str_steps = _dict['steps']
    steps = []
    for step in str_steps:
        steps.append(Steps[step])
    return Pipeline(name,candidate_refs,slave_type,clusters,steps)
