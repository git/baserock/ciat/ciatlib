def log(logfile,component,msg):
    ''' write message to log file with timestamp and script name '''
    import datetime
    _component = str(component)
    _msg = str(msg)
    dt = str(datetime.datetime.now()).split('.')[0]
    to_log = "[%s][%s] %s" % (dt, _component, _msg)
    log_file = open(LOGFILE,'a')
    log_file.write('%s\n' % to_log)
    log_file.close()
